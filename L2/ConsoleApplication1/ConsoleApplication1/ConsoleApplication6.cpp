﻿#include <iostream>
#include <math.h>
#include <stdlib.h>

//C++
double sum(double x[26],double u) {
    int i=0;
    double s = 0;
    int l;
    
    if (u == 0){
        while (i < l){
            s = s + x[i];
            i++;
        }  
    }
    else  if (u == 1) {
        while (i < l) {
            s = s + (i+1)*x[i];
            i++;
        }
    }
    return s;
}

double fun(double m, double n) {
    int i = 1;
    double s = 0;
    while(i <= n) {
        s = s + 1 / (m - i);
        i++;
    }
    return s;
}

int main()
{
    const int n = 26;
    double x[] = { 9, 12, 11, 4, 7, 2, 5, 8, 5, 7, 1, 6, 1, 9, 4, 1, 3, 3, 6, 1, 11, 33, 7, 91, 2, 1 };
    double sx, isx, a, fn, gn, i;
    int l = sizeof(x[n]);
    sx = sum(x[26], 0);
    isx = sum(x[26], 1);
    
    a = sx / isx;
    if (a <= ((n + 1) / 2)) {
        std::cout << "Оценка максимального правдоподобия не имеет решения\n";
    }
       else 
    {
        std::cout << "Оценка максимального правдоподобия имеет единственное решения\n";
        i = n + 1;
        fn = fun(i,n);
        gn = n / (i - a);
        while (fn > gn){
            i = i + 1;
            fn = fun(i, n);
            gn = n / (i - a);
        }
        double B = i - 2;
        std::cout << "Оценка B=",B;
        double K = n / ((B + 1) * sx - isx);
        std::cout << "\nОценка K=", K;
        double xn1 = 1 / (K * (B - n));
        std::cout << "\nСреднее время появленияn+1 ошибки =", xn1, "дней";
        double tk = fun(6, 5) / K;
        std::cout << "\nВремя до окончания тестирования tk=", tk, "дней";
    }
    }
   
